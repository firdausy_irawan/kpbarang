<?php
include 'header.php';
function mysql_result($res, $row, $field=0) { 
    $res->data_seek($row); 
    $datarow = $res->fetch_array(); 
    return $datarow[$field]; 
}
?>

<h3><span class="glyphicon glyphicon-user"></span>  Data Reseller</h3>
<button style="margin-bottom:20px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span>Tambah Reseller</button>
<br/>
<br/>

<?php 
$per_hal=10;
$jumlah_record=mysqli_query($conn,"SELECT COUNT(*) from reseller");
$jum=mysql_result($jumlah_record, 0);
$halaman=ceil($jum / $per_hal);
$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
$start = ($page - 1) * $per_hal;
?>

<div class="col-md-12">
	<table class="col-md-2">
		<tr>
			<td>Jumlah Record</td>		
			<td><?php echo $jum; ?></td>
		</tr>
		<tr>
			<td>Jumlah Halaman</td>	
			<td><?php echo $halaman; ?></td>
		</tr>
	</table>

	<!-- <a style="margin-bottom:10px" href="#" target="_blank" class="btn btn-default pull-right"><span class='glyphicon glyphicon-print'></span>  Cetak</a> -->
</div>

<form action="cari_reseller_act.php" method="get">
	<div class="input-group col-md-5 col-md-offset-7">
		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
		<input type="text" class="form-control" placeholder="Cari reseller di sini .." aria-describedby="basic-addon1" name="cari">	
	</div>
</form>

<br/>
<table class="table table-hover">
	<tr>
		<th class="col-md-1">No</th>
		<th class="col-md-3">Nama Reseller</th>
		<th class="col-md-3">Username</th>
		<th class="col-md-2">No Hp</th>
		<!-- <th class="col-md-1">Sisa</th>		 -->
		<th class="col-md-3">Opsi</th>
	</tr>
	<?php 
	if(isset($_GET['cari'])){
		$cari=mysqli_real_escape_string($conn,$_GET['cari']);
		$reseller=mysqli_query($conn,"select * from reseller where nama_reseller like '%$cari%' or username like '%$cari%'");
	}else{
		$reseller=mysqli_query($conn,"select * from reseller limit $start, $per_hal");
	}
	$no=1;
	while($b=mysqli_fetch_array($reseller)){

		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $b['nama_reseller'] ?></td>
			<td><?php echo $b['username'] ?></td>
			<td><?php echo $b['no_hp'] ?></td>
			<td>
				<a href="detail_reseller.php?id=<?php echo $b['id_reseller']; ?>" class="btn btn-info">Detail</a>
				<a href="edit_reseller.php?id=<?php echo $b['id_reseller']; ?>" class="btn btn-warning">Edit</a>
				<a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='hapus_reseller.php?id_reseller=<?php echo $b['id_reseller']; ?>' }" class="btn btn-danger">Hapus</a>
			</td>
		</tr>		
		<?php 
	}
	?>
</table>

<ul class="pagination">			
			<?php 
			for($x=1;$x<=$halaman;$x++){
				?>
				<li><a href="?page=<?php echo $x ?>"><?php echo $x ?></a></li>
				<?php
			}
			?>						
		</ul>
<!-- modal input -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Reseller Baru</h4>
			</div>
			<div class="modal-body">
				<form onsubmit="return checkPassword(this)" id="tambahReseller" action="tambah_reseller_act.php" method="post">
					<div class="form-group">
						<label>Nama Reseller</label>
						<input name="nama" type="text" class="form-control" placeholder="Nama Reseller ..">
					</div>
					<div class="form-group">
						<label>Nomer Hp</label>
						<input name="nomerHp" type="text" class="form-control" placeholder="Nomer Hp ..">
					</div>
					<div class="form-group">
						<label>Username</label>
						<input name="username" type="text" class="form-control" placeholder="Username ..">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input name="pass1" type="password" class="form-control" placeholder="Password">
					</div>	
					<div class="form-group">
						<label>Konfirmasi Password</label>
						<input name="pass2" type="password" class="form-control" placeholder="Konfirmasi Password">
					</div>															

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<input type="submit" class="btn btn-primary" value="Simpan">
				</div>
			</form>
		</div>
	</div>
</div>

<script>
        function checkPassword(form) { 
                var password1 = form.pass1.value;
                var password2 = form.pass2.value;
                var nama  = form.nama.value;
                var nomerHp  = form.nomerHp.value;
                var username  = form.username.value;
                

                if(nama == ''){
                    alert ("Nama Belum diisi !"); 
                    return false;
                }

                else if (nomerHp == ''){ 
                    alert ("nomer Hp Belum diisi !"); 
                    return false;
                }

                else if (username == ''){ 
                    alert ("username Belum diisi !"); 
                    return false;
                }

                // If password not entered 
                else if (password1 == ''){ 
                    alert ("Password Belum diisi !"); 
                    return false;
                }

                // If confirm password not entered 
                else if (password2 == ''){ 
                    alert ("Konfirmasi password Belum diisi !"); 
                    return false;
                }

                // If Not same return False.     
                else if (password1 != password2) { 
                    alert ("\nKonfirmasi Password Salah !") 
                    return false; 
                } else{
                    return true;
                }
            } 

</script>

<?php 
include 'footer.php';
?>